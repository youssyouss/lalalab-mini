const path = require('path'); 

const express = require('express'); 

const adminController = require('../controllers/admin'); 
const isAuth = require('../middleware/is-auth'); 

const router = express.Router(); 

// GET admin/upload

router.get('/upload', isAuth, adminController.getAddProduct); 

router.get('/products', isAuth, adminController.getProducts); 

router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

// POST admin/add-product

router.post('/upload', isAuth, adminController.postAddProduct);

router.post('/edit-product', isAuth, adminController.postEditProduct);

router.post('/delete-product', isAuth, adminController.postDeleteProduct);

module.exports = router;