# LALALAB. Test - MiNi

Node js api with a little bit of front rendering

E-commerce web app with the following features: 
    - Auth as admin
    - Add, delete, edit product
    - Add, delet product to cart
    - Place order
    - Previous orders list 

## Installation

Use npm package manager to install depedencies

```bash
npm install
```

## Usage

Run development server with nodemon

```
npm run dev
```

Start locally 

```
npm start
```